﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype
{
    class Car : Toy
    {
        public Car(string name)
       : base(name, "Машинка")
        { }
        public override Toy MyClone()
        {
            return new Car(Name);
        }
        
    }
}
