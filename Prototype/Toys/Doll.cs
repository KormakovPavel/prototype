﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype
{
    class Doll : Toy
    {
        public Doll(string name)
       : base(name, "Кукла")
        { }
        public override Toy MyClone()
        {
            return new Doll(Name);
        }
        
    }
}
