﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype
{
    class Toy : IMyCloneable<Toy>, ICloneable
    {
        public string Name { get; set; }
        private readonly string _typeToy;

        public Toy(string name, string typeToy)
        {
            Name = name;
            _typeToy = typeToy;
        }

        public override string ToString()
        {
            return $"Название игрушки: {Name}, тип: {_typeToy}";
        }

        public virtual Toy MyClone()
        {
            return new Toy(Name, _typeToy);
        }

        public virtual object Clone()
        {
            return MyClone();
        }
    }
}
