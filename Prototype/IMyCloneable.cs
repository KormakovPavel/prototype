﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype
{
    interface IMyCloneable<T>
    {
        T MyClone();
    }
}
