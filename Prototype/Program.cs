﻿using System;

namespace Prototype
{
    class Program
    {
        static void Main(string[] args)
        {
            Book AgathaChristie = new DetectiveNovel("Убийство в Восточном экспрессе");
            Book Detective = AgathaChristie.MyClone();
            AgathaChristie.Name = "Тайна замка Чимниз";

            Console.WriteLine(AgathaChristie.ToString());
            Console.WriteLine(Detective.ToString());

            Book MarkTven = new Adventure("Приключения Тома Сойера");
            Book Adventure = (Adventure)MarkTven.Clone();
            MarkTven.Name = "Приключения Гекльберри Финна";

            Console.WriteLine(MarkTven.ToString());
            Console.WriteLine(Adventure.ToString());

            Toy Barbi = new Doll("Барби");
            Toy Doll = Barbi.MyClone();
            Doll.Name = "Маша";

            Console.WriteLine(Barbi.ToString());
            Console.WriteLine(Doll.ToString());

            Toy BMW = new Car("BMW X5");
            Toy Car = (Toy)BMW.Clone();
            Car.Name = "Audi Q7";

            Console.WriteLine(BMW.ToString());
            Console.WriteLine(Car.ToString());

            Console.ReadLine();
        }
    }
}
