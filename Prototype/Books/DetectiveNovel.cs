﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype
{
    class DetectiveNovel : Book
    {
        public DetectiveNovel(string name)
        : base(name, "Детектив")
        { }
        public override Book MyClone()
        {
            return new DetectiveNovel(Name);
        }      

    }
}
