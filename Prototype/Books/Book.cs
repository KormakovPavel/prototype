﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype
{
    class Book : IMyCloneable<Book>, ICloneable
    {
        public string Name { get; set; }
        private readonly string _genre;

        public Book(string name, string genre)
        {
            Name = name;
            _genre = genre;
        }

        public override string ToString()
        {
            return $"Название книги: {Name}, жанр: {_genre}";
        }

        public virtual Book MyClone()
        {
            return new Book(Name, _genre);
        }

        public virtual object Clone()
        {
            return MyClone();
        }
    }
}
