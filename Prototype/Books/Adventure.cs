﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype
{
    class Adventure : Book
    {
        public Adventure(string name)
       : base(name, "Приключение")
        { }
        public override Book MyClone()
        {
            return new Adventure(Name);
        }
        
    }
}
